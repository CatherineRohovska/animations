//
//  ViewController.h
//  Animation
//
//  Created by User on 10/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)shakeClick:(id)sender;
- (IBAction)bounceClick:(id)sender;
- (IBAction)twoAnimationsClick:(id)sender;


@end

