//
//  MessageViewController.m
//  Animation
//
//  Created by User on 10/19/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "MessageViewController.h"
#include <stdlib.h>
@interface MessageViewController ()
{
    int w;
    int h;
}
@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor purpleColor];
   
    w = arc4random_uniform(self.view.bounds.size.width-100);
    h = 50+arc4random_uniform(self.view.bounds.size.height/2);
    
    [self createNextButton];
//

}
- (IBAction) clickMeButton: (UIButton *) sender
{
   // int r = arc4random_uniform(74);
    if ([sender tag]==1)
    {
       
        [UIView animateWithDuration:2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [sender setTitle:@"Bye!" forState:UIControlStateNormal];
            [sender setTitleColor:[UIColor magentaColor] forState:UIControlStateNormal];
           // sender.layer.opacity = 0.1;
            //
            CGMutablePathRef thePath = CGPathCreateMutable();
            float x = sender.layer.position.x;
            float y = sender.layer.position.y;
            w = arc4random_uniform(self.view.bounds.size.width-100);
            h = 100+arc4random_uniform(self.view.bounds.size.height/2);
            CGPathMoveToPoint(thePath,NULL,x,y);
            CGPathAddCurveToPoint(thePath,NULL,x+30.0,y+50.0,
                                  w-30.0,w-50.0,
                                  w,h);
            CGPathAddCurveToPoint(thePath,NULL,x+30.0,y+50.0,
                                  w-56.0,h-50.0,
                                  w,h);
            
            CAKeyframeAnimation * theAnimation;
            
            // Create the animation object, specifying the position property as the key path.
            theAnimation=[CAKeyframeAnimation animationWithKeyPath:@"position"];
            theAnimation.path=thePath;
            theAnimation.duration=2.0;
            // Add the animation to the layer.
            [sender.layer addAnimation:theAnimation forKey:@"position"];
            //
        } completion:^(BOOL finished){
        }];
         sender.transform = CGAffineTransformIdentity;
        [UIView animateWithDuration:2 delay:0.1 options:UIViewAnimationOptionCurveEaseOut animations:^{
            sender.transform = CGAffineTransformMakeScale(0.01, 0.01);
        } completion:^(BOOL finished){
            sender.hidden = YES;
            [sender removeFromSuperview];
            [self createNextButton];
        }];
    }
}
-(void) createNextButton
{
    
    UIButton* button =[[UIButton alloc] initWithFrame:CGRectMake(w, h, 100, 100)];
    [button setTitle:@"Click me!" forState:UIControlStateNormal];
    button.tag = 1;
    button.layer.borderWidth = 5;
    button.layer.borderColor = [UIColor redColor].CGColor;
    [button addTarget:self action:@selector(clickMeButton:) forControlEvents:UIControlEventTouchUpInside];
       button.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [self.view addSubview:button];
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        button.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
       
    }];
}
//

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
